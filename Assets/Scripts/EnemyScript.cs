using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyScript : MonoBehaviour
{
    public float speed = 1;
    public GameObject target;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        //Vector3 targetMovement = new Vector3(0, 0, Input.GetAxis("Vertical"));
        //Enemy.MovePosition(transform.position + targetMovement * Time.deltaTime * speed);
        if (transform.position.z < target.transform.position.z)
        {
            transform.position += new Vector3(0,0,speed)*Time.deltaTime;
        } else if(transform.position.z > target.transform.position.z)
        {
            transform.position -= new Vector3(0, 0, speed) * Time.deltaTime;
        }
    }
}
