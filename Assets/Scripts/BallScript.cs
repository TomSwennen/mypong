using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallScript : MonoBehaviour
{
    public float speed = 1.0f;
    [SerializeField] Rigidbody Ball;
    [SerializeField] float Velocity;

    void Start()
    {
        Ball = Ball.GetComponent<Rigidbody>();

        var euler = transform.eulerAngles;
        euler.x = Random.Range(0.0f, 360.0f);
        euler.z = Random.Range(0.0f, 360.0f);
        //transform.eulerAngles = euler;
        //new Vector3(0, euler.y, 0);

        Ball.AddForce(new Vector3(euler.x, 0, euler.z) * speed * Time.deltaTime, ForceMode.VelocityChange);
    }

    // Update is called once per frame
    void Update()
    {
        Velocity = Ball.velocity.magnitude;
    }

    private void OnCollisionEnter(Collision collision)
    {
        

        if (collision.collider.CompareTag("Pad"))
        {
            Ball.AddForce(Vector3.zero * 2.1f * Time.deltaTime, ForceMode.VelocityChange);
        }
    }
}
