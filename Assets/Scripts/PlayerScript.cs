using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerScript : MonoBehaviour
{
    [SerializeField] float verticalInput;
    public float speed = 1;
    Rigidbody Player;

    void Start()
    {
        verticalInput = Input.GetAxis("Vertical");
        Player = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        verticalInput = Input.GetAxis("Vertical");

        //Vector3 Movement = new Vector3(0, 0, verticalInput);
        //transform.Translate(Movement * speed * Time.deltaTime);

        //transform.Translate(Vector3.forward * verticalInput * speed * Time.deltaTime);


        Vector3 m_Input = new Vector3(0, 0, Input.GetAxis("Vertical"));
        Player.MovePosition(transform.position + m_Input * Time.deltaTime * speed);


    }
}
